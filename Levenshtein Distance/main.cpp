//
//  main.cpp
//  Levenshtein Distance
//
//  Created by Archangel on 15.03.2019.
//  Copyright © 2019 Archangel. All rights reserved.
//

#include <iostream>
#include <string>
#include <cstring>
#define streams freopen("input.txt", "r", stdin); freopen("output.txt", "w", stdout);
using namespace std;

char check(char s)
{
    if ((int)s >= 48 && (int)s <= 57)
        return s;
    
    if ((int)s == 32)
        return s;
    
    if ((int)s >= 97 && (int)s <= 122)
        return s;
    
    if ((int)s >= 65 && (int)s <= 90)
    {
        s = (char)((int (s)) + 32);
        return s;
    }
    
    s = '\0';
    return s;
}

//Normalize string, leaving only eng letters and numbers
//and words minimum length is 4
string normalize(string a)
{
    int n = 0;
    string s = "";
    string output = "";
    for (size_t i = 0; i < a.length(); i++)
    {
        if (check(a[i]) != '\0')
        {
            if (check(a[i]) != ' ')
            {
                s += check(a[i]);
                n++;
            }
            else
            {
                if (n > 3)
                {
                    s += check(a[i]);
                    output += s;
                }
                s = "";
                n = 0;
            }
        }
    }
    
    if (n > 3)
        output += s;
    
    return output;
}

//counting levenshtein distance
size_t uiLevenshteinDistance(const std::string &s1, const std::string &s2)
{
    const size_t m(s1.size());
    const size_t n(s2.size());
    
    if (m == 0) return n;
    if (n == 0) return m;
    
    size_t *costs = new size_t[n + 1];
    
    for(size_t k=0; k<=n; k++)
        costs[k] = k;
    
    size_t i = 0;
    for (string::const_iterator it1 = s1.begin(); it1 != s1.end(); ++it1, ++i)
    {
        costs[0] = i+1;
        size_t corner = i;
        
        size_t j = 0;
        for (string::const_iterator it2 = s2.begin(); it2 != s2.end(); ++it2, ++j)
        {
            size_t upper = costs[j+1];
            if(*it1 == *it2)
            {
                costs[j+1] = corner;
            }
            else
            {
                size_t t(upper<corner?upper:corner);
                costs[j+1] = (costs[j]<t?costs[j]:t)+1;
            }
            corner = upper;
        }
    }
    
    size_t result = costs[n];
    delete [] costs;
    
    return result;
}

int main()
{
    streams;
    ios::sync_with_stdio(false);
    cin.tie(0); cout.tie(0);
    
    string s1 = "";
    getline(cin, s1);
    
    string s2 = "";
    getline(cin, s2);
    
    string o1 = normalize(s1);
    string o2 = normalize(s2);
    
    cout << uiLevenshteinDistance(o1, o2);
    return 0;
}
